# High-Tune: Mie

This library loads and manages the Mie's data

## How to build

This program relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build.
It also depends on the
[RSys](https://gitlab.com/vaplv/rsys/), and
[NetCDF](https://www.unidata.ucar.edu/software/netcdf/) C libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake](https://cmake.org/documentation) for further
informations on CMake.

## Licenses

Copyright (C) 2018, 2020, 2021 [|Meso|Star](http://www.meso-star.com)
<contact@meso-star.com>. Copyright (C) 2018 Centre National de la Recherche
Scientifique (CNRS), Université Paul Sabatier
<contact-edstar@laplace.univ-tlse.fr>. HTMie is free software released under
the GPL v3+ license: GNU GPL version 3 or later. You are welcome to
redistribute it under certain conditions; refer to the COPYING file for
details.

